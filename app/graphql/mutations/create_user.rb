module Mutations
  class CreateUser < BaseMutation
    # often we will need input types for specific mutation
    # in those cases we can define those input types in the mutation class itself

    argument :name, String, required: true
    argument :email, String, required: true
    argument :password, String, required: true

    field :user, Types::UserType, null: true
    field :token, String, null: true
    field :errors, [String], null: false

    def resolve(name: nil, email: nil, password: nil)
      user = User.new(name: name, email: email, password: password)
      if user.save
          @authtoken = Authtoken.create(user_id: user.id)
          {
              user: user,
              token: @authtoken.id,
              errors: []
          }
      else
        puts user.errors.full_messages
          {
              user: nil,
              token: nil,
              errors: user.errors.full_messages
          }
      end
    end
  end
end