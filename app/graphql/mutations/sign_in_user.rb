module Mutations
  class SignInUser < BaseMutation
    null true

    argument :email, String, required: false
    argument :password, String, required: false

    field :user, Types::UserType, null: true
    field :token, String, null: true
    field :errors, [String], null: false

    def resolve(email: nil, password: nil)
      # basic validation
      return unless email && password

      @user = User.find_by email: email

      # ensures we have the correct user
      return unless @user
      return unless @user.authenticate(password)

      # use Ruby on Rails - ActiveSupport::MessageEncryptor, to build a token
      @authtoken = Authtoken.create(user_id: @user.id)

      { user: @user, token: @authtoken.id, errors: [] }
    end
  end
end