module Mutations
  class SignOutUser < BaseMutation

    field :success, String, null: true
    field :errors, [String], null: false

    def resolve()
      puts 0
      @authtoken = Authtoken.find(context[:authtoken])

      if @authtoken.blank?
        puts 1
        { success: "", errors: ["Could not find authtoken to delete"] }
      end
      puts 5
      if @authtoken.destroy
        puts 6
        { success: "Successful signout", errors: [] }
      else
        puts 7
        { success: "Could not destroy authtoken", errors: @authtoken.errors }
      end

    end
  end
end