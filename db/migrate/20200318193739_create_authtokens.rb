class CreateAuthtokens < ActiveRecord::Migration[6.0]
  def change
    enable_extension 'pgcrypto'

    create_table :authtokens, id: :uuid  do |t|
      t.integer :user_id
      t.timestamps
    end
  end
end
