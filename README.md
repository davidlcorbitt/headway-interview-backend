# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

setup:

1. Ensure that you have postgresql installed

2. Login by typing `psql postgres`

3. Create interview role with `CREATE ROLE interview SUPERUSER`

4. Create development database with `CREATE DATABASE interview_backend_development;`
